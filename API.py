#import pfsense_vshell
import requests
import argparse
import json
import uuid
import sys
import os
import signal
import time
#from urllib3.exceptions import InsecureRequestWarning
import fwutils
import fwglobals
import traceback


def _json_parse(data):
    try:
        return json.loads(data)
    except json.JSONDecodeError:
        pass
    #raise PfsenseFauxapiException('Unable to parse response data!', data)

def make_request(method, params, rest):
    # Local variables
    method = params.get("method", method)    # Allow custom method override

    # Create authentication payload for local authentication
    url = fwglobals.g.cfg.API_URL + rest
    auth = {"client-id": fwglobals.g.cfg.CLIENT_ID, "client-token": fwglobals.g.cfg.CLIENT_TOKEN}
    # url = 'https://127.0.0.1/api/v1' + rest
    # auth = {"client-id": 'admin', "client-token": 'admin'}
    params["payload"] = params.get("payload", {})
    params["payload"].update(params.get("auth_payload", auth))
    headers = {}
    print(url)
    try:
        req = requests.request(
            params.get("method", method),
            url=url,
            data=json.dumps(params.get("payload", {})),
            verify=False,
            headers=headers
        )

    except Exception as e:
        pass
    # except (requests.exceptions.ConnectTimeout, requests.exceptions.ReadTimeout):
    #     # print(self.__format_msg__(method, params, "Exceeded timeout of {t}s".format(t=self.args.timeout)))
    #     # return None
    #     pass

    # If this is a request only execution, just return the request/response object
    
    return req


def Read_GateWay():
    return make_request('GET', {}, '/status/gateway')


def Read_Interfaces():
    return make_request('GET', {}, '/interface')

def Create_Interfaces(Data):
    
    res = make_request('POST', Data, '/interface')
    return res

def Modify_Interfaces(Data):

    res = make_request('PUT', Data, '/interface')
    return res

def Delete_Interfaces(Data):

    res = make_request('DELETE', Data, '/interface')
    return res

def Apply_Interfaces(Data):

    res = make_request('POST', Data, '/interface/apply')
    return res

def add_remove_interfaces(is_add, dev_id, ip=None, gw=None, metric=None, dhcp=None, type=None, dnsServers=None, dnsDomains=None, mtu=None, if_name=None, validate_ip=False):
    
    if_name = fwutils.dev_id_to_freebsd_if(dev_id)
    print('add remove interface')
    descr = type
    # dhcp = 'no' if dhcp == '' else dhcp
    #print(dhcp)
    if dhcp == 'yes':
        ip_type = 'dhcp'
        ip = ''
        subnet = ''
    elif dhcp == 'no':
        ip_type = 'staticv4'
        ip_list = ip.split('/')
        ip = ip_list[0]
        subnet = ip_list[1]

    #get a valid description
    descr = descr + '-' + if_name
    
    if is_add:
    
        data = {
                "name": "add or remove interface",
                "payload": {
                    "if": if_name,
                    "descr": descr,
                    "enable": True,
                    "type": ip_type,
                    "type6": "dhcp6",
                    "ipaddr": ip,
                    "ipaddrv6": "",
                    "subnet": subnet,
                    "subnetv6": "",
                    'mtu': mtu,
                    "blockbogons": True,
                }
        }
        res = Create_Interfaces(data)
    else:
        data = {
            "name": "Delete interface",
            "payload": {
                "if": if_name
            }
        }
        res = Delete_Interfaces(data)

    print(res.text)
    ress = _json_parse(res.text)
    if res.status_code != 200 and 'Interface ID in use' in ress['message']:
        data['payload'].pop('if')
        new_id = {'id': if_name}
        data['payload'].update(new_id)
        res = Modify_Interfaces(data)
    
    print(res.text)
    if res.status_code == 200 and is_add:
        res1 = Apply_Interfaces({})
        print(res1.text)

    # try:
    #     ress = _json_parse(res.text)
    #     #print(ress)
    #     if res.status_code != 200 and ress['message'] != 'cannot be deleted' and ress['status'] != 'bad request':
    #         pass
    #         #raise Exception('Unable to complete {}() request'.format('add_remove_netplan_interface'), ress['message'])
    #     fwglobals.log.debug(ress)

    # except Exception as e:
    #     fwglobals.log.error(if_name + str(e))
    #     print(if_name + str(e))
    #     #_, _, exc_traceback_obj = sys.exc_info()
    #     #traceback.print_stack()
    #     return (False, if_name + str(e))

    return (True, None)



#---------------------------------------

def modify_dhcpd(is_add, params):
    """
    add dhcp server to a given lan interface

    interface: "LAN(em1,.....)"
    ip_start: string
    ip_end: string
    dns_addr: string
    static_mapping_list: [{'host': '6.6.6.6', 'mac': '22:22:22:22:22:22', 'ipv4': '192.168.1.1'}]
    """
    print('modify_dhcpd')
    interface = params['interface']
    interface = fwutils.dev_id_to_freebsd_if(interface)
    range_start = params['range_start']
    range_end = params['range_end']
    dns = params['dns']
    static_mapping_list = params['mac_assign']
    if is_add == 1:
        # Update DHCPd Service Configuration
        dhcp_update_data = {
            "name": "add dhcp server to a given lan interface",
            "payload": {
                "interface": interface,
                "enable": True,
                "range_from": range_start,
                "range_to": range_end,
                "dnsserver": dns
            }
        }
        res = make_request('PUT', dhcp_update_data, '/services/dhcpd')
        #print(res.text)
        # Create new DHCPd static mappings
        for static_mapping in static_mapping_list:
            create_static_mapping_data = {
                "name": "Create DHCPd static mapping on the LAN interface",
                "payload": {
                    "interface": interface,
                    "mac": static_mapping['mac'],
                    "ipaddr": static_mapping['ipv4'],
                    "hostname": static_mapping['host'],
                }
                #"resp_time": 5  # Allow a few seconds to reload the DHCP service
            }
            res = make_request('POST', create_static_mapping_data,
                               '/services/dhcpd/static_mapping')
            #print(res.text)
    else:
        # Delete all static mappings
        read_static_mapping_data = {
            "name": "Read all DHCPd static mappings on the specific interface",
            "payload": {
                "interface": interface
            }
        }
        res = make_request('GET', read_static_mapping_data,
                           '/services/dhcpd/static_mapping')
        static_mappings = _json_parse(res.text)['data']
        for static_mapping in static_mappings:
            delete_static_mapping_data = {
                "name": "Delete DHCPd static mapping on the LAN interface",
                "payload": {
                    "interface": interface,
                    "id": static_mapping['id']
                },
                "resp_time":
                5  # Allow a few seconds to reload the DHCP service
            }
            res = make_request('DELETE', delete_static_mapping_data,
                               '/services/dhcpd/static_mapping')
        # check
        read_static_mapping_data = {
            "name": "Read all DHCPd static mappings on the specific interface",
            "payload": {
                "interface": interface
            }
        }
        res = make_request('GET', read_static_mapping_data,
                           '/services/dhcpd/static_mapping')
        static_mappings = _json_parse(res.text)['data']
        if len(static_mappings) > 0:
            for static_mapping in static_mappings:
                delete_static_mapping_data = {
                    "name": "Delete DHCPd static mapping on the LAN interface",
                    "payload": {
                        "interface": interface,
                        "id": static_mapping['id']
                    },
                    "resp_time":
                    5  # Allow a few seconds to reload the DHCP service
                }
                res = make_request('DELETE', delete_static_mapping_data,
                                   '/services/dhcpd/static_mapping')

        # Disable DHCPd Service for the specific interface
        dhcp_update_data = {
            "name": "Disable DHCPd Service for the specific interface",
            "payload": {
                "interface": interface,
                "enable": False,
            }
        }
        res = make_request('PUT', dhcp_update_data, '/services/dhcpd')

    return (True, None)



def add_static_route(addr, via, remove, metric=None,dev_id=None):
    """
    添加静态路由

    dest: 目标网络: 如10.3.8.0/24
    via: 网关， 决定下一跳
    metric: 质量
    """
    print('add static route')

    res = Read_GateWay()
    res = _json_parse(res.text)['data']
    gateway_name = ''
    for gateway in res:
        if via == gateway['monitorip']:
            gateway_name = gateway['name']
            break

    if remove == False:
        # Create Static Routes
        create_static_route_data = {
            "name": "Create static route",
            "payload": {
                "network": addr,
                "gateway": gateway_name,
                "disabled": False,
                "apply": True
            }
        }
        res = make_request("POST", create_static_route_data,
                           "/routing/static_route")
        print(res.text)
    else:
        #Delete existing static routes.
        read_static_route_data = {"name": "Read all static routes"}
        res = make_request("GET", read_static_route_data,
                           "/routing/static_route")
        static_routes = _json_parse(res.text)['data']
        for static_route in static_routes:
            if static_route['network'] == addr and static_route[
                    'gateway'] == via:
                id = static_route['id']
                break
        delete_static_route_data = {
            "name": "Delete static route",
            "payload": {
                "id": 0
            },
            "resp_time": 5  # Allow a few seconds to reload the routing table
        }
        res = make_request("DELETE", delete_static_route_data,
                           "/routing/static_route")
        print(res.text)
    return (True, None)


def set_DNS(interface, DNS_Server, DNS_Domains):
    '''
    给interface配置DNS。
    '''
    pass


def Read_Interfaces_Status():
    return _json_parse(make_request('GET', {}, '/status/interface').text)


# if __name__ == '__main__':
#     data = {
#                 "name": "add or remove interface",
#                 "payload": {
#                     "id": 'em0',
#                     "descr": 'WAN',
#                     "enable": True,
#                     "type": 'dhcp',
#                     "type6": "staticv6",
#                     "ipaddr": '',
#                     "ipaddrv6": "2001:0db8:85a3:0000:0000:8a2e:0370:7334",
#                     "subnet": "",
#                     "subnetv6": "120",
#                     "blockbogons": True,
#                 }
#         }
    
#     # res = add_remove_interfaces(0, 'pci:0000:02:01.00')
#     # print(res.text)
#     print(Read_Interfaces_Status())
#     #print(_json_parse(Read_GateWay().text)['name'])


    

    
