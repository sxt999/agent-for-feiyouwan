#! /usr/bin/python3

################################################################################
# flexiWAN SD-WAN software - flexiEdge, flexiManage.
# For more information go to https://flexiwan.com
#
# Copyright (C) 2019  flexiWAN Ltd.
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Affero General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
################################################################################

# Handle device statistics
import fwikev2
import fwutils
import math
import time
import loadsimulator
import psutil
import subprocess
from fwtunnel_stats import tunnel_stats_get
import fwglobals
import fwtunnel_stats
import API

# Globals
# Keep updates up to 1 hour ago
UPDATE_LIST_MAX_SIZE = 120

# Keeps the list of last updates
updates_list = []

# Keeps the VPP pids
vpp_pid = ''

# Keeps last stats
stats = {'ok':0, 'running':False, 'last':{}, 'bytes':{}, 'tunnel_stats':{}, 'health':{}, 'period':0}

def update_stats():
    """Update statistics dictionary using values retrieved from VPP interfaces.

    :returns: None.
    """
    global stats
    global vpp_pid

    # devId = {'em0':'pci:0000:02:01.00', 'em1':'pci:0000:02:02.00', 'em2':'pci:0000:02:03.00',
    # 'em3':'pci:0000:02:04.00', 'em4':'pci:0000:02:05.00','em5':'pci:0000:02:06.00'}
    new_stats = API.Read_Interfaces_Status()
    # print(',,,,,,,,,,,,,,,,,,,,,')
    # print(new_stats['data'])
    # if not new_stats:
    #     stats['ok'] = 0
    #else:
    prev_stats = dict(stats)  # copy of prev stats
    stats['time'] = time.time()
    stats['last'] = new_stats['data']
    stats['ok'] = 1
    # Update info if previous stats valid
    if stats['ok'] == 1:
        if_bytes = {}

        for devv in stats['last']:
            intf = devv['hwif']
            prev_stats_if = {}
            for dev in prev_stats.get('last', {}):
                if intf == dev['hwif']:
                    prev_stats_if = dev
                    break
            # print('last...................')
            # print(prev_stats_if)
            if not prev_stats_if:
                prev_rx_bytes = devv['inbytespass']
                prev_rx_pkt = devv['inpktspass']
                prev_tx_bytes = devv['outbytespass']
                prev_tx_pkt = devv['outpktspass']
                prev_stats['time'] = time.time()
            
            else:
                prev_rx_bytes = prev_stats_if['inbytespass']
                prev_rx_pkt = prev_stats_if['inpktspass']
                prev_tx_bytes = prev_stats_if['outbytespass']
                prev_tx_pkt = prev_stats_if['outpktspass']
            
            #if prev_stats_if != {}:
            rx_bytes = 1.0 * (devv['inbytespass'] - prev_rx_bytes)
            rx_pkts  = 1.0 * (devv['inpktspass'] - prev_rx_pkt)
            tx_bytes = 1.0 * (devv['outbytespass'] - prev_tx_bytes)
            tx_pkts  = 1.0 * (devv['outpktspass'] - prev_tx_pkt)
            
            calc_stats = {
                    'rx_bytes': rx_bytes,
                    'rx_pkts': rx_pkts,
                    'tx_bytes': tx_bytes,
                    'tx_pkts': tx_pkts
            }
            #print(intf)
            dev_id = fwutils.get_freebsd_interface_dev_id(intf)
            
            if_bytes[dev_id] = calc_stats
            
        #print(prev_stats)
        if prev_stats.get('time', None) == None:
            prev_stats['time'] = time.time()
        stats['bytes'] = if_bytes
        #stats['tunnel_stats'] = tunnel_stats
        stats['period'] = stats['time'] - prev_stats['time']
        stats['running'] = True #if fwutils.vpp_does_run() else False
        # print(',,,,,,,,,,,,')
        # print(if_bytes)
    # Add the update to the list of updates. If the list is full,
    # remove the oldest update before pushing the new one
    if len(updates_list) is UPDATE_LIST_MAX_SIZE:
        updates_list.pop(0)

    updates_list.append({
            'ok': stats['ok'], 
            'running': stats['running'], 
            'stats': stats['bytes'], 
            'period': stats['period'],
            'tunnel_stats': {},  #TODO
            'health': get_system_health_(),
            'utc': time.time()
        })
    

def get_system_health_():
    # Get CPU info
    try:
        cpu_stats = psutil.cpu_percent(percpu = True)
    except Exception as e:
        fwglobals.log.excep("Error getting cpu stats: %s" % str(e))
        cpu_stats = [0]
    # Get memory info
    try:
        memory_stats = psutil.virtual_memory().percent
    except Exception as e:
        fwglobals.log.excep("Error getting memory stats: %s" % str(e))
        memory_stats = 0
    # Get disk info
    try:
        disk_stats = psutil.disk_usage('/').percent
    except Exception as e:
        fwglobals.log.excep("Error getting disk stats: %s" % str(e))
        disk_stats = 0
    # Get temperature info
    try:
        temp_stats = {'value':0.0, 'high':100.0, 'critical':100.0}
        all_temp = psutil.sensors_temperatures()
        for ttype, templist in list(all_temp.items()):
            if ttype == 'coretemp':
                temp = templist[0]
                if temp.current: temp_stats['value'] = temp.current
                if temp.high: temp_stats['high'] = temp.high
                if temp.critical: temp_stats['critical'] = temp.critical
    except Exception as e:
        fwglobals.log.excep("Error getting temperature stats: %s" % str(e))

    return {'cpu': cpu_stats, 'mem': memory_stats, 'disk': disk_stats, 'temp': temp_stats}


def get_system_health():
    # Get CPU info
    try:
        cpu_stats = psutil.cpu_percent(percpu = True)
    except Exception as e:
        fwglobals.log.excep("Error getting cpu stats: %s" % str(e))
        cpu_stats = [0]
    # Get memory info
    try:
        memory_stats = psutil.virtual_memory().percent
    except Exception as e:
        fwglobals.log.excep("Error getting memory stats: %s" % str(e))
        memory_stats = 0
    # Get disk info
    try:
        disk_stats = psutil.disk_usage('/').percent
    except Exception as e:
        fwglobals.log.excep("Error getting disk stats: %s" % str(e))
        disk_stats = 0
    # Get temperature info
    try:
        temp_stats = {'value':0.0, 'high':100.0, 'critical':100.0}
        all_temp = psutil.sensors_temperatures()
        for ttype, templist in list(all_temp.items()):
            if ttype == 'coretemp':
                temp = templist[0]
                if temp.current: temp_stats['value'] = temp.current
                if temp.high: temp_stats['high'] = temp.high
                if temp.critical: temp_stats['critical'] = temp.critical
    except Exception as e:
        fwglobals.log.excep("Error getting temperature stats: %s" % str(e))

    return {'cpu': cpu_stats, 'mem': memory_stats, 'disk': disk_stats, 'temp': temp_stats}


def get_stats():
    """Return a new statistics dictionary.

    :returns: Statistics dictionary.
    """
    res_update_list = list(updates_list)
    print(',,,,,,,,,,,,,,,,,,,,')
    print(res_update_list)
    del updates_list[:]

    reconfig = fwutils.get_reconfig_hash()
    ikev2_certificate_expiration = fwglobals.g.ikev2.get_certificate_expiration()

    # If the list of updates is empty, append a dummy update to
    # set the most up-to-date status of the router. If not, update
    # the last element in the list with the current status of the router
    if loadsimulator.g.enabled():
        status = True
        state = 'running'
        reason = ''
        reconfig = ''
    else:
        status = True
        state = 'running'
        reason = ''
    if not res_update_list:
        info = {
            'ok': stats['ok'],
            'running': status,
            'state': state,
            'stateReason': reason,
            'stats': {},
            'tunnel_stats': {},
            'health': {},
            'period': 0,
            'utc': time.time(),
            'reconfig': reconfig
        }
        if fwglobals.g.ikev2.is_private_key_created():
            info['ikev2'] = ikev2_certificate_expiration
        res_update_list.append(info)
    else:
        res_update_list[-1]['running'] = status
        res_update_list[-1]['state'] = state
        res_update_list[-1]['stateReason'] = reason
        res_update_list[-1]['reconfig'] = reconfig
        res_update_list[-1]['health'] = get_system_health()
        if fwglobals.g.ikev2.is_private_key_created():
            res_update_list[-1]['ikev2'] = ikev2_certificate_expiration
    
    return {'message': res_update_list, 'ok': 1}

def get_stats_():
    """Return a new statistics dictionary.

    :returns: Statistics dictionary.
    """
    res_update_list = list(updates_list)
    del updates_list[:]

    reconfig = fwutils.get_reconfig_hash()
    ikev2_certificate_expiration = fwglobals.g.ikev2.get_certificate_expiration()

    # If the list of updates is empty, append a dummy update to
    # set the most up-to-date status of the router. If not, update
    # the last element in the list with the current status of the router
    if loadsimulator.g.enabled():
        status = True
        state = 'running'
        reason = ''
        reconfig = ''
    else:
        status = True if fwutils.vpp_does_run() else False
        (state, reason) = fwutils.get_router_state()
    if not res_update_list:
        info = {
            'ok': stats['ok'],
            'running': status,
            'state': state,
            'stateReason': reason,
            'stats': {},
            'tunnel_stats': {},
            'health': {},
            'period': 0,
            'utc': time.time(),
            'reconfig': reconfig
        }
        if fwglobals.g.ikev2.is_private_key_created():
            info['ikev2'] = ikev2_certificate_expiration
        res_update_list.append(info)
    else:
        res_update_list[-1]['running'] = status
        res_update_list[-1]['state'] = state
        res_update_list[-1]['stateReason'] = reason
        res_update_list[-1]['reconfig'] = reconfig
        res_update_list[-1]['health'] = get_system_health_()
        if fwglobals.g.ikev2.is_private_key_created():
            res_update_list[-1]['ikev2'] = ikev2_certificate_expiration

    return {'message': res_update_list, 'ok': 1}

def update_state(new_state):
    """Update router state field.

    :param new_state:         New state.

    :returns: None.
    """
    stats['running'] = new_state

def reset_stats():
    """Reset statistics.

    :returns: None.
    """
    global stats
    stats = {'running': False, 'ok':0, 'last':{}, 'bytes':{}, 'tunnel_stats':{}, 'health':{}, 'period':0, 'reconfig':False, 'ikev2':''}